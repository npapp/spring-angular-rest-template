package com.norbcorp.hungary.product.backend;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

@SessionScope
@Component
public class ProductManager implements Serializable {

  private List<Product> productList = null;

  public List<Product> getAllProducts(){
    if(productList == null ) {
        productList = new LinkedList<>();
        productList.add(new Product(1l, "name1", "description1", 100, 1.0f));
        productList.add(new Product(2l, "name2", "description2", 100, 1.0f));
        productList.add(new Product(3l, "name3", "description3", 100, 1.0f));
        productList.add(new Product(4l, "name4", "description4", 100, 1.0f));
        productList.add(new Product(5l, "name5", "description5", 100, 1.0f));
    }
    return productList;
  }

  public List<Product> getProductList() {
    return productList;
  }

  public void addProduct(Product product){
    productList.add(product);
  }
}
