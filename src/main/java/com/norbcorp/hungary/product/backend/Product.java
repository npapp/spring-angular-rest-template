package com.norbcorp.hungary.product.backend;

import java.io.Serializable;
import java.util.Objects;

public class Product implements Serializable {
  private Long id;
  private String productName;
  private String description;
  private Integer price;
  private Float rating;

  public Product() {
  }

  public Product(Long id, String productName, String description, Integer price, Float rating) {
    this.id = id;
    this.productName = productName;
    this.description = description;
    this.price = price;
    this.rating = rating;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Integer getPrice() {
    return price;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }

  public Float getRating() {
    return rating;
  }

  public void setRating(Float rating) {
    this.rating = rating;
  }

  @Override
  public String toString() {
    return "Product{" +
      "id=" + id +
      ", productName='" + productName + '\'' +
      ", description='" + description + '\'' +
      ", price=" + price +
      ", rating=" + rating +
      '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Product product = (Product) o;
    return Objects.equals(id, product.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
