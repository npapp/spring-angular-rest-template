package com.norbcorp.hungary.product.frontend;

import com.norbcorp.hungary.product.backend.Product;
import com.norbcorp.hungary.product.backend.ProductManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

@RestController
public class ProductController implements Serializable {

  private Logger logger = Logger.getLogger(ProductController.class.getName());

  @Autowired
  private ProductManager productManager;

  @GetMapping("/all")
  public List<Product> getAllProducts(){
      return productManager.getAllProducts();
  }

  @PostMapping(value = "/add", consumes = "application/json")
  public void addProduct(@RequestBody Product product){
    if(product!=null && product.getProductName()!=null && product.getDescription()!=null) {
      product.setId(productManager.getAllProducts().size() + 1l);
      productManager.addProduct(product);
    }
  }

  @DeleteMapping(value="/delete/{id}")
  public void deleteProduct(@PathVariable(value="id") final Long id){
    if(id!=null)
      productManager.getAllProducts().removeIf((p)->p.getId().equals(id));
  }

  @PutMapping(value="/put", consumes = "application/json")
  public void changeProduct(@RequestBody Product product){
    if(productManager.getAllProducts().contains(product)) {
      productManager.getAllProducts().removeIf((p) -> p.getId().equals(product.getId()));
      productManager.addProduct(product);
    }
  }
}
