import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,  HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Product } from '../product/product.component';
import { catchError, retry } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};


@Injectable()
export class ProductService {

  constructor(private http: HttpClient){}

  getAllProducts(): Observable<Product[]> {
    console.log('Getting all products from the service');
    return this.http.get<Product[]>('/all');
  }

  getSpecifiedProduct(id: number): Observable<Product>{
    return this.http.get<Product>(`/get/${id}`);
  }

  addProduct(product : Product) {
    this.http.post<Product>('/add', product, httpOptions).
    subscribe(
       val => {
           console.log("POST call successful value returned in body",
                       val);
       },
       response => {
           console.log("POST call in error", response);
       },
       () => {
           console.log("The POST observable is now completed.");
       });
  }

  modifyProduct(product: Product){
    this.http.put<Product>('/put', product, httpOptions).
    subscribe(
        val => {
            console.log("PUT call successful value returned in body",
                        val);
        },
        response => {
            console.log("PUT call in error", response);
        },
        () => {
            console.log("The PUT observable is now completed.");
        });
  }

  deleteProduct(id: number){
   return this.http.delete(`/delete/${id}`).
      subscribe(
             val => {
                 console.log("DELETE call successful value returned in body",
                             val);
             },
             response => {
                 console.log("DELETE call in error", response);
             },
             () => {
                 console.log("The DELETE observable is now completed.");
             }
         );
  }
}

