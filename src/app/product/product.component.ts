import { Component, OnInit, Inject } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ProductService } from './product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  allProducts: Product[];
  constructor(private productService: ProductService) { }
  selectedProduct: Product;

  ngOnInit() {
    this.productService.getAllProducts().subscribe(
      (data: Product[]) => this.allProducts = data,
      (err: any) => console.log(err)
    );
  }

  getAllProducts(): Product[]{
    return this.allProducts;
  }

  deleteProduct(selected: Product){
    this.productService.deleteProduct(selected.id);
    this.allProducts = this.allProducts.filter(obj => obj !== selected);
  }

  modifyProduct(product: Product){
   this.selectedProduct = product;
  }
}

export interface IProduct {

}
export class Product implements IProduct{
     id: number;
     productName: string;
     description: string;
     price: number;
     rating: number;

     setId(id: number){
        this.id=id;
     }
}
