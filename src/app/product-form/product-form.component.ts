import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../product/product.component';
import { ProductService } from '../product/product.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

  private model: Product;
  productList: Product[];

  constructor(private productService: ProductService) { }

  ngOnInit() {
     this.model = new Product();
  }

  @Input()
  set selectedProduct(product: Product){
    this.model=product;
  }

  @Input()
  set allProducts(allProducts: Product[]){
    this.productList = allProducts;
  }

  add(){
    if(this.model && typeof this.model.productName!='undefined' && this.model.productName!='' && this.model.productName!=null &&
     typeof this.model.description!='undefined' && typeof this.model.description!=null){
      this.productService.addProduct(this.model);
      this.model.setId(this.productList[this.productList.length-1].id+1);
      this.productList.push(this.model);
      this.model = new Product();
      this.model.id=null;
      this.model.productName='';
      this.model.description='';
    }
  }

  change(){
    if(this.model){
      this.productService.modifyProduct(this.model);
      this.model=new Product();
      this.model.id=null;
      this.model.productName='';
      this.model.description='';
    }
  }
}
